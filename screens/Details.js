import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  Alert,
} from "react-native";
import axios from "axios";
import { LinearGradient } from "expo-linear-gradient";

export default function details({ route, navigation }) {
  const [cuaca, setCuaca] = useState("Loading...");
  const [waktu, setWaktu] = useState("");
  const [bg, setBg] = useState(["white", "transparent"]);
  const [color, setColor] = useState("black");
  const [tintColor, setTintColor] = useState("black");
  const [transform, setTransform] = useState([{ rotate: `1deg` }]);
  
  let { nama, kota } = route.params;//receive params send from Start page

  useEffect(() => {
    getWeather();
    getTime();
  }, []);

  const time = new Date();
  const weekday = [
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu",
  ];
  const month = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  //time data manipulation
  const hari = weekday[time.getDay()];
  const tanggal = time.getDate();
  const bulan = month[time.getMonth()];
  const tahun = time.getFullYear();
  const jam = time.getHours();//mengambil jam kita akan komen dulu
  //const jam =21 //kalau malam
  const today = hari + ", " + tanggal + " " + bulan + " " + tahun;

  const index = nama.indexOf(" "); 
  if (index !== -1) {               //check if nama has more than one word
    nama = nama.substring(0, index);//set nama to only the first word
  }

  function getTime() {  //changing environment based on time like background color, font color, etc
    if (jam >= 4 && jam < 10) {
      setWaktu("Pagi");
      setBg(["#00BFFF", "orange"]);
      setTransform([{ rotate: `${cuaca.windDegNow}deg` }]);
    } else if (jam >= 10 && jam < 14) {
      setWaktu("Siang");
      setBg(["#87CEEB", "#00BFFF"]);
      setTransform([{ rotate: `${cuaca.windDegNow}deg` }]);
    } else if (jam >= 14 && jam < 19) {
      setWaktu("Sore");
      setBg(["#87CEEB", "gold", "red"]);
      setTransform([{ rotate: `${cuaca.windDegNow}deg` }]);
    } else {
      setWaktu("Malam");
      setBg(["black", "#0000CD"]);
      setColor("white");
      setTintColor("white");
      setTransform([{ rotate: `${cuaca.windDegNow}deg` }]);
    }
  }

  function getWeather() {//get weather data from api with variable kota as a input
    axios
      .get(
        `http://api.openweathermap.org/data/2.5/forecast?q=${kota},id&lang=id&units=metric&appid=8bcbccb6fa755f0e545cf16b1dc8834a`
      )
      .then((res) => {
        //const main = res.data
        setCuaca({ //set cuaca with data from API response
          windDegNow: res.data.list[0].wind.deg,
          tempNow: res.data.list[0].main.temp,
          weatherNow: res.data.list[0].weather[0].main,
          weatherNowDesc: res.data.list[0].weather[0].description,
          weatherIconNow: res.data.list[0].weather[0].icon,
          humidityNow: res.data.list[0].main.humidity,
          pressureNow: res.data.list[0].main.pressure,
          cloudinessNow: res.data.list[0].clouds.all,
          windSpeedNow: res.data.list[0].wind.speed,
          city: res.data.city.name,
          forecast: res.data.list,
        });
        console.log(res.data.list[0].wind.deg);
      })
      .catch(function (error) {//error handle when kota is not found
        console.log(error);
        Alert.alert(
          "Not Found",
          "Kota/Kabupaten tidak ditemukan.\nTekan Cancel untuk kembali",
          [
            {
              text: "Cancel",
              onPress: () => navigation.popToTop(),
              style: "cancel",
            },
          ]
        );
      });
  }

  return (
    <>
      <LinearGradient
        colors={bg}
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          bottom: 0,
          top: 0,
        }}
      />
      <View>
        <View style={styles.flexBoxTransparent}>
          <TouchableOpacity
            onPress={() => {
              navigation.popToTop();
            }}
          >
            <Image
              style={[styles.stackIcon, { tintColor }]}
              source={require("../assets/back.png")}
            />
          </TouchableOpacity>
          <View style={{ alignItems: "center" }}>
            <Text style={{ fontSize: 30, ...{ color } }}>{cuaca.city}</Text>
            <Text style={{ ...{ color } }}>{today}</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              getWeather();
            }}
          >
            <Image
              style={[styles.stackIcon, { tintColor }]}
              source={require("../assets/reload.png")}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.flexBoxTransparent}>
          <View style={{ alignItems: "center" }}>
            <Text style={{ ...{ color } }}>
              Selamat {waktu}, {nama}
            </Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Text style={{ fontSize: 40, ...{ color } }}>
                {cuaca.tempNow}
              </Text>
              <Text style={{ fontSize: 20, ...{ color } }}>{"\xB0C"}</Text>
            </View>
          </View>
          <View style={{ alignItems: "center" }}>
            <Text style={{ ...{ color } }}>{cuaca.weatherNow}</Text>
            <Image
              style={{ height: 80, width: 80 }}
              source={{
                uri: `http://openweathermap.org/img/w/${cuaca.weatherIconNow}.png`,
              }}
            />
            <Text style={{ ...{ color } }}>{cuaca.weatherNowDesc}</Text>
          </View>
        </View>
        <View style={styles.flexBox}>
          <View style={{ alignItems: "center" }}>
            <Image
              style={[styles.mainIcon, { tintColor }]}
              source={require("../assets/humidity.png")}
            />
            <Text style={{ ...{ color } }}>{cuaca.humidityNow}%</Text>
            <Text style={[styles.mainFont, { color }]}>Kelembaban</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Image
              style={[styles.mainIcon, { tintColor }]}
              source={require("../assets/pressure.png")}
            />
            <Text style={{ ...{ color } }}>{cuaca.pressureNow} mPa</Text>
            <Text style={[styles.mainFont, { color }]}>Tekanan</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Image
              style={[styles.mainIcon, { tintColor }]}
              source={require("../assets/cloud.png")}
            />
            <Text style={{ ...{ color } }}>{cuaca.cloudinessNow}%</Text>
            <Text style={[styles.mainFont, { color }]}>Ketebalan</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Image
              style={[styles.mainIcon, { tintColor }, { transform }]}
              source={require("../assets/wind.png")}
            />
            <Text style={{ ...{ color } }}>{cuaca.windSpeedNow} m/s</Text>
            <Text style={[styles.mainFont, { color }]}>Angin</Text>
          </View>
        </View>
        <Text
          style={{
            fontSize: 20,
            margin: 10,
            alignSelf: "center",
            ...{ color },
          }}
        >
          Prakiraan Cuaca
        </Text>
        <View style={styles.flexBox2}>
          <SafeAreaView>
            <FlatList
              data={cuaca.forecast}
              horizontal={true}
              scrollEnabled={true}
              keyExtractor={(item) => item.dt.toString()}
              renderItem={({ item }) => {
                return (
                  // <>
                  <View style={{ alignItems: "center", margin: 5 }}>
                    <Text style={{ ...{ color } }}>
                      {weekday[new Date(item.dt * 1000).getDay()].slice(0, 3)}
                    </Text>
                    <Text style={{ ...{ color } }}>
                      {new Date(item.dt * 1000).getDate().toString()}/
                      {new Date(item.dt * 1000).getMonth().toString()}
                    </Text>
                    <Text style={{ ...{ color } }}>
                      {new Date(item.dt * 1000).toString().slice(16, 21)}
                    </Text>
                    <Image
                      style={{ height: 60, width: 60 }}
                      source={{
                        uri: `http://openweathermap.org/img/w/${item.weather[0].icon}.png`,
                      }}
                    />
                    <Text style={{ ...{ color } }}>
                      {item.main.temp.toString()}
                      {"\xB0C"}
                    </Text>
                  </View>
                  //</>
                );
              }}
            />
          </SafeAreaView>
          <Text
            style={{ alignSelf: "center", ...{ color } }}
          >{`<--- scroll ---->`}</Text>
        </View>
        <Text style={{ fontSize: 10, ...{ color } }}>
          *all icon in this app provided by{" "}
          <Text style={{ fontWeight: "bold" }}>icon8</Text>
        </Text>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  flexBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 5,
    padding: 5,
    borderRadius: 10,
    backgroundColor: "#00000030",
  },
  flexBox2: {
    //flexDirection: "column",
    //justifyContent: "center",
    //alignItems: 'center',
    margin: 5,
    padding: 5,
    borderRadius: 10,
    backgroundColor: "#00000030",
  },
  flexBoxTransparent: {
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 5,
    padding: 5,
    borderRadius: 10,
  },
  mainIcon: {
    height: 50,
    width: 50,
  },
  stackIcon: {
    height: 30,
    width: 30,
  },
  mainFont: {
    fontSize: 12,
  },
});
