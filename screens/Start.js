import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
  View,
  Image,
} from "react-native";
import axios from "axios";
import Autocomplete from "react-native-dropdown-autocomplete-textinput";

export default function Start({ navigation }) {
  const [fullName, setFullName] = useState("");
  const [province, setProvince] = useState("");
  const [city, setCity] = useState("");
  const [dataProvince, setDataProvince] = useState([]);
  const [dataCity, setDataCity] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    getProvince();
  }, []);

  async function getProvince() {//get list data of province data from API
    await axios
      .get("http://www.emsifa.com/api-wilayah-indonesia/api/provinces.json")
      .then((res) => {
        const data1 = res.data;
        console.log("fetch provinsi success");//log when fetch success
        setDataProvince(data1);
      })
      .catch(function (error) {
        console.log(error);
        setError("Koneksi Gagal");//log when fetch failed
      });
  }

  function getCity(prov) {//get list data of regencies/cities based on selected province from API
    axios
      .get(
        `http://www.emsifa.com/api-wilayah-indonesia/api/regencies/${prov}.json`
      )
      .then((res) => {
        const data2 = res.data;
        console.log("fetch kota success");
        setDataCity(data2);
      })
      .catch(function (error) {
        console.log(error);
        setError("Koneksi Gagal");
      });
  }

  function handleProvinceChange(item) {
    const prov = item.id;
    console.log(prov);
    setProvince(item.name); //set province to selected province
    getCity(prov);
  }
  function handleCityChange(item) {
    let kota = item.name;
    const index = kota.indexOf(" ");
    kota = kota.substring(index); //set province to selected province with first word removed
    setCity(kota);                 //example: KABUPATEN INDRAMAYU --> INDRAMAYU 
  }

  function handleSubmit() {
    if (fullName != "" && city != "" && province != "") {//check all fields filled
      console.log(fullName, city, province);
      navigation.navigate("DETAILS", {  //navigate to DETAILS page
        nama: fullName,                 //send param to DETAILS page
        kota: city,
      });
    } else setError("Data Belum Lengkap"); //error handle when a field empty
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1, padding: 5 }}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={{ flex: 1, justifyContent: "flex-end" }}>
          <Image
            style={{ height: 150, width: 150, alignSelf: "center" }}
            source={require("../assets/cloud.png")}
          />
          <Text style={{ alignSelf: "center", fontSize: 30, marginBottom: 30 }}>
            OSN WeatherApp
          </Text>
          <Text style={{ color: "#034EA2", fontStyle: "italic" }}>
            Masukkan Nama Lengkap
          </Text>
          <TextInput
            value={fullName}
            onChangeText={(fullName) => setFullName(fullName)}
            style={{
              margin: 5,
              padding: 5,
              borderWidth: 1,
              borderColor: "transparent",
              borderBottomColor: "#C7C1CB",
              height: 35,
            }}
          />
          <Autocomplete
            floatBottom={true}
            maxHeight={200}
            data={dataProvince}
            displayKey="name"
            placeholder={"Masukkan Nama Propinsi"}
            onSelect={(item) => handleProvinceChange(item)}
          />
          <Autocomplete
            floatBottom={true}
            maxHeight={200}
            data={dataCity}
            displayKey="name"
            placeholder={"Masukkan Nama Kabupaten/Kota"}
            onSelect={handleCityChange}
          />
          <View style={{ height: 70, padding: 10 }}>
            <Text>{error}</Text>
          </View>
          <TouchableOpacity
            style={{
              padding: 10,
              margin: 5,
              borderRadius: 10,
              backgroundColor: "#00B207",
              height: 50,
              justifyContent: "center",
            }}
            activeOpacity={0.7}
            onPress={handleSubmit}
          >
            <Text
              style={{
                fontSize: 17,
                color: "#FFD700",
                fontWeight: "bold",
                alignSelf: "center",
              }}
            >
              MASUK
            </Text>
          </TouchableOpacity>
          <Text style={{ fontSize: 10 }}>
            *all icon in this app provided by{" "}
            <Text style={{ fontWeight: "bold" }}>icon8</Text>
          </Text>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({});
