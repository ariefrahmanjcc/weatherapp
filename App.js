import React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import Router from "./router/Router";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <Router />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    backgroundColor: "white",
    //justifyContent: 'space-around',
    //padding: 10,
    //alignItems: ''
  },
});
