# WeatherApp

Menampilkan Cuaca suatu wilayah dan prakiraannya 5 hari ke depan 

## Fitur
- Ganti ganti background berdasarkan waktu (pagi, siang, sore, malam)
- sapaan selamat berdasarkan waktu (pagi, siang, sore, malam)
- muncul alert jika kota yang dipilih tidak tidak ditemukan

## Link Demo Aplikasi

https://youtu.be/rHFNfML3ZWQ

## Link file .apk

https://drive.google.com/file/d/1IvOk9KesMloz6K_bFZTyQGZnakLU7_Ka/view?usp=sharing

## Biodata Developer
Nama: Arief Rahman
Kelas: React Native

## Credits to
https://icon8.com for icon